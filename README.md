# Facets link field processors

This module provides facets processors specific to the core link type field.

## Processors

* `TranslateEntityInLinkProcessor` - transforms the links that point to entities 
into their labels in the facets results.
