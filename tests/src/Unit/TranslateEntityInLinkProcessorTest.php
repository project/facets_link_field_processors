<?php

namespace Drupal\Tests\facets_link_field_processors\Unit;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\facets\Entity\Facet;
use Drupal\facets\FacetInterface;
use Drupal\facets_link_field_processors\Plugin\facets\processor\TranslateEntityInLinkProcessor;
use Drupal\facets\Result\Result;
use Drupal\link\LinkItemInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Unit test for the "translate_entity_in_link" processor.
 *
 * @group facets_link_field_processors
 */
class TranslateEntityInLinkProcessorTest extends UnitTestCase {

  /**
   * The mocked language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected LanguageManagerInterface|MockObject $languageManager;

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected EntityTypeManagerInterface|MockObject $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock language manager.
    $this->languageManager = $this->createMock(LanguageManagerInterface::class);
    $language = new Language(['langcode' => 'en']);
    $this->languageManager->expects($this->any())
      ->method('getCurrentLanguage')
      ->willReturn($language);

    // Mock entity type manager.
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);

    // Create and set a global container with the language manager and entity
    // type manager.
    $container = new ContainerBuilder();
    $container->set('language_manager', $this->languageManager);
    $container->set('entity_type.manager', $this->entityTypeManager);
    \Drupal::setContainer($container);
  }

  /**
   * Provides mock data for the tests in this class.
   *
   * @return array
   *   The facet and results test data.
   */
  public function facetDataProvider(): array {
    $data = [];
    $map = [
      LinkItemInterface::LINK_GENERIC => TRUE,
      LinkItemInterface::LINK_INTERNAL => TRUE,
      LinkItemInterface::LINK_EXTERNAL => FALSE,
    ];

    foreach ($map as $link_type => $supported) {
      $data_definition = $this->createMock(FieldItemDataDefinition::class);
      $data_definition->expects($this->any())
        ->method('getDataType')
        ->willReturn('field_item:link');
      $data_definition->expects($this->any())
        ->method('getSetting')
        ->withAnyParameters()
        ->willReturn($link_type);

      // Create the actual facet.
      $facet = $this->createMock(Facet::class);
      $facet->expects($this->any())
        ->method('getDataDefinition')
        ->willReturn($data_definition);

      // Add a field identifier.
      $facet->expects($this->any())
        ->method('getFieldIdentifier')
        ->willReturn('testfield');

      $results = [];
      $results[] = new Result($facet, 'http://example.com', 'http://example.com', 1);
      $results[] = new Result($facet, 'entity:node/1', 'entity:node/1', 1);
      $results[] = new Result($facet, 'entity:taxonomy_term/2', 'entity:taxonomy_term/2', 1);
      $facet->setResults($results);

      $data[$link_type][] = $facet;
      $data[$link_type][] = $results;
      $data[$link_type][] = $supported;
    }

    return $data;
  }

  /**
   * Tests that results are correctly changed.
   *
   * @param \Drupal\facets\FacetInterface $facet
   *   A facet mock.
   * @param \Drupal\facets\Result\ResultInterface[] $results
   *   The facet original results mock.
   * @param bool $supported
   *   Whether the facet is supported.
   *
   * @dataProvider facetDataProvider
   */
  public function testResultsChanged(FacetInterface $facet, array $results, bool $supported) {
    $processor = new TranslateEntityInLinkProcessor([], 'translate_entity_in_link', [], $this->languageManager, $this->entityTypeManager);
    $this->assertEquals($supported, $processor->supportsFacet($facet));

    // If the facet is not supported, we can stop here.
    if (!$supported) {
      return;
    }

    // Mock a node and add the label to it.
    $node = $this->createMock(Node::class);
    $node->expects($this->any())
      ->method('label')
      ->willReturn('shaken not stirred');

    $node_storage = $this->createMock(EntityStorageInterface::class);
    $node_storage->expects($this->any())
      ->method('load')
      ->willReturn($node);

    // Mock a term and add the label to it.
    $term = $this->createMock(Term::class);
    $term->expects($this->any())
      ->method('label')
      ->willReturn('Burrowing owl');

    $term_storage = $this->createMock(EntityStorageInterface::class);
    $term_storage->expects($this->any())
      ->method('load')
      ->willReturn($term);

    $this->entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->willReturnMap([
        ['node', $node_storage],
        ['taxonomy_term', $term_storage],
      ]);

    // Without the processor we expect the original link URIs to show.
    $result_values = [];
    foreach ($results as $result) {
      $result_values[$result->getRawValue()] = $result->getDisplayValue();
    }
    $this->assertEquals([
      'http://example.com' => 'http://example.com',
      'entity:node/1' => 'entity:node/1',
      'entity:taxonomy_term/2' => 'entity:taxonomy_term/2',
    ], $result_values);

    // Process the results and assert the link to the node was transformed.
    $processor = new TranslateEntityInLinkProcessor([], 'translate_entity_in_link', [], $this->languageManager, $this->entityTypeManager);
    /** @var \Drupal\facets\Result\ResultInterface[] $filtered_results */
    $filtered_results = $processor->build($facet, $results);
    $result_values = [];
    foreach ($filtered_results as $result) {
      $result_values[$result->getRawValue()] = $result->getDisplayValue();
    }

    $this->assertEquals([
      'http://example.com' => 'http://example.com',
      'entity:node/1' => 'shaken not stirred',
      'entity:taxonomy_term/2' => 'Burrowing owl',
    ], $result_values);

    // Process the results again, this time removing the results that are not
    // links.
    $processor = new TranslateEntityInLinkProcessor([
      'remove_non_entities' => TRUE,
    ], 'translate_entity_in_link', [], $this->languageManager, $this->entityTypeManager);
    /** @var \Drupal\facets\Result\ResultInterface[] $filtered_results */
    $filtered_results = $processor->build($facet, $results);
    $result_values = [];
    foreach ($filtered_results as $result) {
      $result_values[$result->getRawValue()] = $result->getDisplayValue();
    }

    $this->assertEquals([
      'entity:node/1' => 'shaken not stirred',
      'entity:taxonomy_term/2' => 'Burrowing owl',
    ], $result_values);
  }

}
