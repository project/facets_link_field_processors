<?php

namespace Drupal\facets_link_field_processors\Plugin\facets\processor;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Drupal\link\LinkItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Transforms the results to show the translated entity label.
 *
 * @FacetsProcessor(
 *   id = "facets_link_field_processors",
 *   label = @Translation("Transform entity link to label"),
 *   description = @Translation("Display the entity label instead of its URI from link field."),
 *   stages = {
 *     "build" = 5
 *   }
 * )
 */
class TranslateEntityInLinkProcessor extends ProcessorPluginBase implements BuildProcessorInterface, ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'remove_non_entities' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet): array {
    $form = parent::buildConfigurationForm($form, $form_state, $facet);

    $form['remove_non_entities'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove non entities'),
      '#description' => $this->t('Check this box if you would like to not render the results that do not represent links to entities.'),
      '#default_value' => $this->getConfiguration()['remove_non_entities'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results): array {
    $language_interface = $this->languageManager->getCurrentLanguage();
    $configuration = $this->getConfiguration();

    /** @var \Drupal\facets\Result\ResultInterface $result */
    foreach ($results as $delta => $result) {
      $value = $result->getRawValue();
      $entity = $this->getEntityFromLinkValue($value);
      if (!$entity instanceof ContentEntityInterface) {
        if ((bool) $configuration['remove_non_entities']) {
          unset($results[$delta]);
        }

        continue;
      }

      if ($entity->hasTranslation($language_interface->getId())) {
        $entity = $entity->getTranslation($language_interface->getId());
      }

      $result->setDisplayValue($entity->label());
    }

    // Return the results with the new display values.
    return $results;
  }

  /**
   * Determines and returns the entity from a given link field value.
   *
   * @param string $value
   *   The field value.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity or NULL if no entity could be determined.
   */
  protected function getEntityFromLinkValue(string $value): ?EntityInterface {
    try {
      $url = Url::fromUri($value);
      if (!$url->isRouted()) {
        return NULL;
      }

      $route_parts = explode('.', $url->getRouteName());
      if (count($route_parts) !== 3 || $route_parts[0] !== 'entity' || $route_parts[2] !== 'canonical') {
        return NULL;
      }

      $entity_type = $route_parts[1];
      try {
        $storage = $this->entityTypeManager->getStorage($entity_type);
        return $storage->load($url->getRouteParameters()[$entity_type]);
      }
      catch (PluginNotFoundException $exception) {
        return NULL;
      }
    }
    catch (\Exception $exception) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function supportsFacet(FacetInterface $facet): bool {
    $data_definition = $facet->getDataDefinition();
    if ($data_definition->getDataType() !== 'field_item:link') {
      return FALSE;
    }

    $type = $data_definition->getSetting('link_type');
    if (in_array($type, [LinkItemInterface::LINK_GENERIC, LinkItemInterface::LINK_INTERNAL])) {
      // Only valid for link fields configured to allow also internal URLs.
      return TRUE;
    }

    return FALSE;
  }

}
